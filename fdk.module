<?php

use Drupal\Core\Field\FormatterInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\fdk\FDKHelper;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\Core\Render\Markup;

/**
 * Implements hook_field_formatter_third_party_settings_form().
 */
function fdk_field_formatter_third_party_settings_form(FormatterInterface $plugin, FieldDefinitionInterface $field_definition, $view_mode, $form, FormStateInterface $form_state) {
  return FDKHelper::settingsForm($plugin, $field_definition, $view_mode, $form, $form_state);
}

/**
 * Implements hook_field_formatter_settings_summary_alter()
 */
function fdk_field_formatter_settings_summary_alter(array &$summary, array $context) {
  FDKHelper::formatter_settings_summary_alter($summary, $context);
}

/**
 * Implements hook_theme()
 */
function fdk_theme() {
  return [
    'fdk_settings_summary' => [
      'render element' => 'element',
    ],
  ];
}

/**
 * Prepares settings summary.
 *
 * Default template: fdk-ettings-summary.html.twig.
 */
function template_preprocess_fdk_settings_summary(&$variables) {
  $element = $variables['element'];
  $variables['settings'] = $element['#settings'];
  $variables['settings']['field_wrapper']['html_element_attributes'] = FDKHelper::getAttributesFromConfig($variables['settings']['field_wrapper']['html_element_attributes']);
  $variables['settings']['field_item_wrapper']['html_element_attributes'] = FDKHelper::getAttributesFromConfig($variables['settings']['field_item_wrapper']['html_element_attributes']);
}

/**
 * Implements hook_theme_registry_alter().
 */
function fdk_theme_registry_alter(&$theme_registry) {
  // Only override the field template if it comes from core. Allow
  // theme overrides to take precedence.
  if (strpos($theme_registry['field']['path'], 'core') !== 0) {
    return;
  }
  $module_path = drupal_get_path('module', 'fdk');
  $theme_registry['field']['path'] = $module_path . '/templates';
}


/**
 * Implements hook_preprocess_HOOK() for field templates.
 */
function fdk_preprocess_field(&$variables) {
  $entity = $variables['element']['#object'];
  $token_data = [];
  // Not to add for token replacement if a valid entity is not available.
  // Id will not be present during Layout Builder preview.
  if ($entity->id()) {
    $token_data[$entity->getEntityTypeId()] = $entity;
  }

  if (!isset($variables['title_attributes'])) {
    $variables['title_attributes'] = new Attribute();
  }
  if (!isset($variables['field_wrapper_attributes'])) {
    $variables['field_wrapper_attributes'] = new Attribute();
  }

  $variables['label_tag'] = 'div';
  $variables['field_wrapper_tag'] = 'div';
  $variables['field_item_wrapper_tag'] = 'div';

  if (isset($variables['element']['#third_party_settings']['fdk'])) {
    $settings = FDKHelper::prepareConfigurationSettings($variables['element']['#third_party_settings']['fdk']);

    // Update the label if it is not from the Layout Builder.
    if (!isset($variables['element']['#third_party_settings']['layout_builder']) && !empty($settings['label']['value'])) {
      $variables['label'] = $settings['label']['value'];
    }

    if ($settings['label']['html_element'] != '_default') {
      $attributes = FDKHelper::getAttributesFromConfig($settings['label']['html_element_attributes'] ?? '', $token_data);
      $variables['title_attributes'] = NestedArray::mergeDeep($variables['title_attributes'], $attributes);

      // Create the class attribute array if it doesn't already exist.
      if (!isset($variables['title_attributes']['class'])) {
        $variables['title_attributes']['class'] = [];
      }

      // Add any free-form classes.
      if (!empty($settings['label']['html_element_classes'])) {
        $classes = explode(' ', $settings['label']['html_element_classes']);
        foreach ($classes as $class) {
          $variables['title_attributes']['class'][] = Html::cleanCssIdentifier($class);
        }
      }

      if ($settings['label']['html_element'] == '_other') {
        $variables['label_tag'] = $settings['label']['html_element_other'];
      }
      else {
        $variables['label_tag'] = $settings['label']['html_element'];
      }
    }

    if ($settings['field_wrapper']['html_element'] != '_default') {
      $attributes = FDKHelper::getAttributesFromConfig($settings['field_wrapper']['html_element_attributes'] ?? '', $token_data);
      $variables['field_wrapper_attributes']->merge(new Attribute($attributes));

      // Create the class attribute array if it doesn't already exist.
      if (!isset($variables['field_wrapper_attributes']['class'])) {
        $variables['field_wrapper_attributes']['class'] = [];
      }

      // Add any free-form classes.
      if (!empty($settings['field_wrapper']['html_element_classes'])) {
        $classes = explode(' ', $settings['field_wrapper']['html_element_classes']);
        foreach ($classes as $class) {
          $variables['field_wrapper_attributes']['class'][] = Html::cleanCssIdentifier($class);
        }
      }

      if ($settings['field_wrapper']['html_element'] == '_other') {
        $variables['field_wrapper_tag'] = $settings['field_wrapper']['html_element_other'];
      }
      else {
        $variables['field_wrapper_tag'] = $settings['field_wrapper']['html_element'];
      }

      $variables['multiple'] = (boolean) $settings['field_wrapper']['force_multiple'];
    }

    if ($settings['field_item_wrapper']['html_element'] != '_default') {
      $item_attributes = new Attribute(FDKHelper::getAttributesFromConfig($settings['field_item_wrapper']['html_element_attributes'] ?? '', $token_data));
      // if (!isset($variables['field_item_wrapper_attributes'])) {
      //   $variables['field_item_wrapper_attributes'] = new Attribute();
      // }
      // $attributes = FDKHelper::getAttributesFromConfig($settings['field_item_wrapper']['html_element_attributes'] ?? '', $token_data);
      // $variables['field_item_wrapper_attributes'] = NestedArray::mergeDeep($variables['field_item_wrapper_attributes'], $attributes);

      // Create the class attribute array if it doesn't already exist.
      if (!isset($item_attributes['class'])) {
        $item_attributes ['class'] = [];
      }


      // $item_classes = [];
      // Add any free-form classes.
      if (!empty($settings['field_item_wrapper']['html_element_classes'])) {
        $classes = explode(' ', $settings['field_item_wrapper']['html_element_classes']);
        foreach ($classes as $class) {
          // $variables['field_item_wrapper_attributes']['class'][] = Html::cleanCssIdentifier($class);
          $item_attributes['class'][] = Html::cleanCssIdentifier($class);
        }
      }

      if ($settings['field_item_wrapper']['html_element'] == '_other') {
        $variables['field_item_wrapper_tag'] = $settings['field_item_wrapper']['html_element_other'];
      }
      else {
        if ($settings['field_item_wrapper']['html_element'] == 'a') {
          $item_attributes['href'] = \Drupal::token()->replace($settings['field_item_wrapper']['html_element_link_href'], $token_data);
        }
        $variables['field_item_wrapper_tag'] = $settings['field_item_wrapper']['html_element'];
      }

      foreach ($variables['items'] as $delta => &$item) {
        $item['attributes']->merge($item_attributes);
      }
    }
  }
}

/**
 * Implements hook_form_alter()
 */
function fdk_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if (preg_match('/_layout_builder_form$/', $form_id) || $form_id == 'entity_view_display_edit_form') {
    $output = t('One or more active themes on this site have field templates incompatible with the Field Display Kit (FDK) Module. ');
    if (\Drupal::currentUser()->hasPermission('administer site configuration')) {
      $output .= t('Please see <a href=":reprot_page">this report page</a> for a complete list of incompatible templates and more information.', [':reprot_page' => Url::fromRoute('fdk.reports')->toString()]);
    }
    $rendered_message = Markup::create($output);
    \Drupal::messenger()->addMessage($rendered_message, 'warning', FALSE);
  }
}
