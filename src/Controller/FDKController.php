<?php

namespace Drupal\fdk\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\fdk\FDKHelper;

/**
 * Returns responses for Metatag routes.
 */
class FDKController extends ControllerBase {
  use StringTranslationTrait;

  public function report() {
    $incompatible_template_files = FDKHelper::getIncompatibleFieldTemplates();
    $build = [
      'description' => [
        '#markup' => t("<p>Below is a list of field templates from all active themes that are incompatible with the Field Display Kit module field template file: field.html.twig</p>") .
        t("<p>It may not be necessary to rewrite all of them, but you must modify the templates from the themes that are actively being used on this website so they won't appear in this list and will work correctly with field configurations provided by the Field Display Kit module.  Please refer to the field.html.twig template file within the Field Display Kit module.</p>")
      ],
    ];

    $root =  \Drupal::root();

    foreach ($incompatible_template_files as $theme_name => $files_info) {
      $rows = [];
      foreach ($files_info as $file_info) {
        $rows[] = [substr($file_info->uri, strlen($root) + 1)];
      }
      $theme = \Drupal::service('theme_handler')->getTheme($theme_name);
      $build[$theme_name]['heading'] = [
        '#prefix' => '<h2>',
        '#markup' => $theme->info['name'],
        '#suffix' => '</h2>',
      ];
      $build[$theme_name]['table'] = [
        '#type' => 'table',
        '#header' => [
          ['data' => $this->t('File path')],
        ],
        '#rows' => $rows,
        '#suffix' => '<br /><br />',
        '#caption' => $this->t('All incompatible field templates from the %theme theme.', ['%theme' => $theme->info['name']]),
        '#sticky' => TRUE,
      ];
    }
    return $build;
  }
}